Pick an integer between 1 and 20 inclusive and I will try to guess it

---

1. your number % 2 = 
   - [0](2)
   - [1](3)

2. okay, but your number % 3 = 
   - [0](4)
   - [1](5)
   - [2](6)

3. okay, but your number % 3 =  
   - [0](7)
   - [1](8)
   - [2](9)

4. your number % 5 - 1 =
   - [0](16)
   - [1](22)
   - [2](28)

5. (your number + 1) % 5 =
   - [0](14)
   - [1](20)
   - [2](26)

6. your number % 5 - 2 =
   - [0](12)
   - [1](18)
   - [2](24)
   - [3](30)

7. your number % 5 - 3 =
   - [0](13)
   - [1](19)
   - [2](25)

8. your number % 5 - 1 = 
   - [0](11)
   - [1](17)
   - [2](23)
   - [3](29)

9. your number % 5 = 
   - [0](15)
   - [1](21)
   - [2](27)

10. Placeholder

11. your number is 1

12. your number is 2

13. your number is 3

14. your number is 4

15. your number is 5

16. your number is 6

17. your number is 7

18. your number is 8

19. your number is 9

20. your number is 10

21. your number is 11

22. your number is 12

23. your number is 13

24. your number is 14

25. your number is 15

26. your number is 16

27. your number is 17

28. your number is 18

29. your number is 19

30. your number is 20
